###Prototype###

from abc import ABCMeta, abstractmethod 
import copy 

class Courses_At_GFG(metaclass = ABCMeta): 
	
	# constructor 
	def __init__(self): 
		self.id = None
		self.type = None

	@abstractmethod
	def course(self): 
		pass

	def get_type(self): 
		return self.type

	def get_id(self): 
		return self.id

	def set_id(self, sid): 
		self.id = sid 

	def clone(self): 
		return copy.copy(self) 

class DSA(): 
	def __init__(self): 
		super().__init__() 
		self.type = "Mine 2"

	def do_something(self): 
		print("Mine 2") 

class SDE(): 
	def __init__(self): 
		super().__init__() 
		self.type = "Mine 1"

	def do_something(self): 
		print("Mine 1") 

class STL(): 
	def __init__(self): 
		super().__init__() 
		self.type = "Mine 3"

	def do_something(self): 
		print("Mine 3") 

class TTL(): 
	def __init__(self): 
		super().__init__() 
		self.type = "Mine 4"

	def do_something(self): 
		print("Mine 4") 

class XTL(): 
	def __init__(self): 
		super().__init__() 
		self.type = "Mine 5"

	def do_something(self): 
		print("Mine 5") 

class Courses_At_GFG_Cache: 
	
	cache = {} 

	@staticmethod
	def get_course(sid): 
		COURSE = Courses_At_GFG_Cache.cache.get(sid, None) 
		return COURSE.clone() 

	@staticmethod
	def load(): 
		sde = SDE() 
		sde.set_id("1") 
		Courses_At_GFG_Cache.cache[sde.get_id()] = sde 

		dsa = DSA() 
		dsa.set_id("2") 
		Courses_At_GFG_Cache.cache[dsa.get_id()] = dsa 

		stl = STL() 
		stl.set_id("3") 
		Courses_At_GFG_Cache.cache[stl.get_id()] = stl 

if __name__ == '__main__': 
	Courses_At_GFG_Cache.load() 

	sde = Courses_At_GFG_Cache.get_course("1") 
	print(sde.get_type()) 

	dsa = Courses_At_GFG_Cache.get_course("2") 
	print(dsa.get_type()) 

	stl = Courses_At_GFG_Cache.get_course("3") 
	print(stl.get_type()) 

print("Clone mine:")
k = SDE()
k.do_something()

m = DSA()
m.do_something()

y = STL()
y.do_something()

y = TTL()
y.do_something()

y = XTL()
y.do_something()

